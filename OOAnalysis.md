# OO Analysis #
The process of constructing the domain model is based on the use cases, especially the nouns used, and on the description of the utterance.
## Rationale for identifying domain classes ##
For the identification of domain classes is used the list of categories of TP classes (suggested in the book). As a result we have the following table of concepts (or classes, but not software) by category.

### _List of Categories_ ###

**(Business) Transactions**

* ServiceRequest;
* ServiceProviderApplication
* ExecutionOrder


---

**Lines of transactions**

* SchedulePreference
* ServiceRequestDescription
* OtherCost

---

**Products or services related to transactions**

* Service
* ** Fixed Service **
* ** Limited Service **
* ** Expandable Service **

---

**Records (of transactions)**

*

---

**Roles of people**

* Administrative
* Employee Human Resources (HRH)
* Client
* Service provider
* User
* Unregistered user

---  


**Places**

*  GeographicalArea
*  PostalCode
*  PostalAddress

---

**Events**

----



**Physical objects**

---

**Specifications and descriptions**

* (Specify) Category (Service)
* (Specify) Service
* AcademicQualification
* ProfessionalQualification
* Disponibility
* Type of service

---

**Specifications and descriptions**

---

**Catalogs**

*

---


**Packages**

*

---

**Sets**

*

---

**Elements of sets**

---

**Organization**

*  Company

---

**Other (external) systems**

* (User Management Component)
* External Service

---

**Other systems (external)**

---

**Register (financial), of work, contracts, legal documents**

---

**Financial instruments**

*

---


**Referred documents / to perform the tasks /**


---



### ** Rationale for identifying associations between classes ** ###

An association is a relationship between instances of objects that indicates a relevant connection that is worth remembering, or is derivable from the List of Common Associations:

+ A is physically (or logically) part of B
+ A is physically (or logically) contained in B
+ A is a description of B
+ A is known / captured / recorded by B
+ A uses or generates B
+ A is related to a B + transaction etc.

|Concept (A)|Association|Concept (B)|
|----------|:-------------:|------:|
|Administrator|specifies|Category|
||specifies|Service|
||specifies|Geographical Area|
||works for|Company|
||acts as|User|
|Company|does|Service|
||has|Category|
||acts in|Geographical Area|
||has|Client|
||has|Administrator|
||has|HRO|
||has|Service Provider|
||receives|Request for Provision of Services|
||receives|Application to Service Provider|
||**defines**|**External Service**|
||**has (multiple)**|**Service Type**|
|Service|cataloged in|Category|
||is requested in|Request for Provision of Services|
||referred to in|Service Request Description|
||**is from (one)**|**Service Type**|
|**Fixed Service**|**it's a specialization**|Service|
|**Limited Service**|**it's a specialization**|Service|
|**Expandable Service**|**it's a specialization**|Service|
| Client|has|Postal Address|
||acts as|User|
||realizes|Request for Provision of Services|
|Category|catalogs|Service|
||mentioned in|Application to Service Provider|
||mentioned in|Service Provider|
|Service Provider|acts as|User|
||**indicates (multiple)**|**Availability** |
||**performs services in (multiple)**| **Geographical Area** |
||**performs cataloged services in (multiple)**| **Category** |
|HRO|acts as|User|
|Application to Service Provider|mentions|Postal Address
||mentions|Academic Qualification
||mentions|Professional Qualification
||mentions|Service Category
||has attached|Support Document
|Request for Provision of Services|is realized by|Client|
||includes|Other Aditional Cost|
||indicates|Schedule Preference|
||has|Service Request Description|
||to be held in|Postal Address|
|Service Request Description|is in|Request for Provision of Services|
||referring to|Service|
|Geographical Area|**centered in**|**Postal Code**|
||**resort to**|**External Service**|
||**acts in (multiple)**|**Postal Code**|
|**External Service**|**informs**|**(Distance+PostalCode)**|
|Postal Address|**has (one)**|**Postal Code**|

**Note:** External services provide distance to postal codes. Each information pair Distance + CodePostal is used to establish the association "acts in" between AreaGeografica and CodePostal.

## Domain Model

![MD_LAPR.png](MD_LAPR.png)