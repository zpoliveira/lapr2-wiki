# Glossary

|**TERMS**|**DESCRIPTION**|
|--------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|**Academic Qualification**|The set of data and/or documents that prove the existence of the applicant's academic qualifications.|
|**Administrator**|Person responsible for carrying out various and different activities to support the operation of the company.|
|**Application**|Corresponds to a set of information made available by a person to the company in order to be able to become a service provider of the company in the future.|
|**ApplicationGPSD**|The main program used by the Company.|
|**Authentication Process**|(Or **FacadeAuth**) The means by which the identity of the person intending / using the computer application is verified|
|**Availability**|Represents a set of days and time intervals that the Service Provider determine, for the use of the Company to assing Service Orders for execution. It may have associated a Time Pattern when it's repeatable.|
|**Category**|Corresponds to a description used to catalog one or more (similar) services provided by the company.|
|**Client**|Person who interacts with the application with the main purpose of making requests for service provision (s) and consequently, to enjoy the benefits of those same services.|
|**Company**|Organization that is dedicated to provide various services to the home and for which the computer application is being developed.|
|**Document**|Represents a piece of documentation used for a specific purpose.|
|**Expandable Service**|Corresponds to a type of service similar to the limited service, however the time indicated by the customer may be exceeded to allow the service to be properly completed.|
|**Fixed Service**|Corresponds to a type of service for which the company determines (e.g. based on an estimate) a duration, irrespective of whether it is more or less time consuming to execute. It is the amount of this duration that is always charged to the customer.|
|**Geographical Area**|Corresponds to a certain region of the land surface in which the company operates or can operate (i.e. provide services). **This region is determined through the set of postal codes existing within a radius (e.g. 5 km) around another postal code.** The provision of services in a given geographical area entails a cost of travel.|
|**HRO**|Acronym for Human Resources Officer|
|**Human Resources Officer**|Person responsible for carrying out various and different activities to support the operation of the company.|
|**Limited Service**|Corresponds to a type of service for which there is no predetermined duration and therefore, at the time of its request, the client must stipulate the desired time. These services are executed to the limit of time indicated by the client, after which the service rendered is terminated.|
|**Postal Address**|The set of data that congregates a physical address and a Postal Code. Used by the Client.|
|**Postal Code**|It is an identifier formed by numbers and / or letters, created by the postal administrations of each territory (e.g. country) and that is used to facilitate the identification of the different localities of that same territory. In this sense, it facilitates the spatial location of a postal address.|
|**Professional Qualification**|The set of data and/or documents that prove the applicant's previous professional experience.|
|**Schedule Preference**|The choice of time and date made by the Client for the request of a Service.|
|**Service Provider**|Person responsible for performing the services requested by clients to the company.|
|**Service Request**|Corresponds to a request made by the client to the company in order to provide the requested services within one or more periods of time for a certain (estimated) cost.|
|**Service Type**|It is a qualification attributed to each service provided by the company in order to apply different business rules by type. At the moment, there are only 3 types of service: (i) Fixed Services; (ii) Limited Services; and (iii) Expandable Services.|
|**Service**|It corresponds to the description of an activity / task that the company proposes to provide to its clients by using one or more people (i.e., Service Provider).|
|**Unregistered User**|User interacting with the computer application anonymously, i.e. without having previously performed the intended authentication process.|
|**User ID**|Internal number or code of an organization that serves to univocally identify a collaborator in the company / organization. In particular, in this case it serves to identify the Service Providers.|
|**User Session**|Represents the current user in session, i.e. using the program ApplicationGPSD.|
|**User**|A user who interacts with the computer application after performing the intended authentication process, and therefore the application knows its identity. Typically, this assumes the role / function of Client or Administrative or FRH or Service Provider.|
