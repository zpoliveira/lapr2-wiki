# Use Cases Diagram

![Use Cases Diagram](useCases/UCD.jpg)

## Use Cases

| UC  | Description |
|:----|:------------------------------------------------------------------------|
| UC1 | [Client Registration](useCases/UC1_ClienteRegistration.md)   |
| UC2 | [Service Provider Application Submission](useCases/UC2_ServiceProviderApplicationSubmission.md)  |
| UC3 | [Specify Category](useCases/UC3_SpecifyCategory.md) |
| UC4 | [Specify Service](useCases/UC4_SpecifyService.md) |
| UC5 | [Specify Geographical Area](useCases/UC5_SpecifyGeographicalArea.md) |
| UC6 | [Request Service Request](useCases/UC6_RequestServiceRequest.md) |
| UC7 | [Associate Postal Address](useCases/UC7_AssociatePostalAddress.md) |
| UC8 | [Register Service Provider](useCases/UC8_RegisterServiceProvider.md) |
| UC9 | [Specify Daily Availability](useCases/UC9_SpecifyDailyAvailability.md) |
| UC10 | [Assing Service Providers to a Service Requests](useCases/UC10_AssingServiceProvidersServiceRequests.md) |
| UC11 | [Decide Proposed Period Of Services](useCases/UC11_DecideProposedPeriodOfServices.md) |
| UC12 | [Consult Execution Orders](useCases/UC12_ConsultExecutionOrders.md) |
| UC13 | [Report End of The Work](useCases/UC13_ReportEndWork.md) |
| UC14 | [Rate Service](useCases/UC14_RateService.md) |
| UC15 | [Evaluate Service Provider](useCases/UC15_EvaluateServiceProvider.md) |
