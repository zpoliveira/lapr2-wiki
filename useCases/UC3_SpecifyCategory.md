# UC3 - Specify Category (Service)

## Short Format

The Administrator begins to specify a new Category. The system requests the required data (i.e., unique code and description). The administrator enters the requested data. The system validates and presents the data to the Administrator, asking you to confirm them. The Administrator confirms. The system records the data and informs the management of the success of the operation.

## SSD

![SSD_UC3_LAPR.png](images/SSD_UC3_LAPR.png)

## Long Format

### Main actor

Administrator

### Parties and their interests

* **Administrator:** intends to specify the categories of services so that it can later catalog the various services provided.
* **Client:** cataloging the services into categories facilitates the performance of their actions.
* **Company:** wants to be able to catalog your services.

### Preconditions

n/a

### Post-conditions

Category information is recorded in the system.

## Main success scenario (or basic flow)

1. The Administrator begins to specify a new Category.
2. The system requests the required data (i.e. unique code and description).
3. The Administrator enters the requested data.
4. The system validates and presents the data to the Administrator, asking you to confirm them.
5. The Administrator confirms.
6. The system records the data and informs the management of the success of the operation.

### Extensions (or Alternate Flows)

* **\*The. The Administrator requests the cancellation of the Category specification.**
  * The use case ends.

* **4a. Required minimum data missing.**
  1. The system informs you of missing data.
  2. The system allows the missing data to be entered (step 3)
      * 2a. The Administrator does not change the data. The use case ends.

* **4b. The system detects that the data (or some subset of the data) entered must be unique and already exist in the system.**
  1. The system alerts the Administrator to the fact.
  2. The system allows you to change it (step 3)
      * 2a. The Administrator does not change the data. The use case ends.

* **4c. The system detects that the entered data (or some subset of the data) is invalid.**
  1. The system alerts the Administrator to the fact.
  2. The system allows you to change it (step 3).
      * 2a. The Administrator does not change the data. The use case ends.

### Special Requirements

\ -

### List of Technologies and Data Variations

\ -

### Frequency of Occurrence

\ -

### Open questions

* Are there other data that are needed?
* What are the mandatory data for specifying a Category?
* What data together allow you to detect duplication of categories?
* How often does this use case occur?
