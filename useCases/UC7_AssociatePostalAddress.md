# UC7 – Associate a Postal Address to a Client

## Short Format

The client initiates the association of a new postal address to his info. The System requests de necessary data (postal address). The Client introduces the requested data. The System validates and shows the data, requesting confirmation. The Client confirms. The System **associates de postal address to the client** and reports the operation success.

## SSD

![SSD_UC7_LAPR.png](images/SSD_UC7_LAPR.png)

## Long Format

### Main Actor

Client

### Parties and their interests

+ **Client:** wishes to associate a new postal address to his list of addresses.

+ **Company:** wishes that the client is capable of requesting a new service order to any of his addresses.

### Preconditions

+ Be authenticated as a Client.

### Post Conditions

The Clients postal address is saved in the system.

## Main success scenario (or basic flow)

1. O cliente inicia a associação de um novo endereço postal à sua informação.The client initiates the association of a new postal address to his info.
2. O sistema solicita os dados necessários (i.e. endereço postal).The System requests de necessary data (postal address).
3. O Cliente introduz os dados solicitados. The Client introduces the requested data.
4. O sistema valida e apresenta os dados, pedindo que os confirme.The System validates and shows the data, requesting confirmation.
5. The Client confirms.
6. The System **associates de postal address to the client** and reports the operation success.

### Extensions (or alternative flows)

+ **\*a. The Client request the canceling the association request.**
    1. The use case ends.

+ **4a. Minimum obligatory data missing.**
    1. The system informs what data is missing.
    2. The system allows for the introduction of new data (step 2).
        + 2a. The Client alters the data. The use case ends.

+ **4b. The system detects that the inserted data (or a subset of the data) should be unique and that it already exists in the system.**
    1. The system alerts the Client of this issue.
    2. The system allows it's correction (step 2)
        + 2a. The Client does not change the data. The use case ends.

+ **4c. The system detects that the inserted data (or a subset of the data) are invalid.**
    1. The system alerts the Client of this issue.
    2. The system allows it's correction (step 2)
        + 2a. The Client does not change the data. The use case ends.

### Special Requisites

\-

### Technology and Data Variations

\-

### Frequency of Occurrence

\-

### Open Issues

+ Which set of data should allow the detection of address duplication?
+ Should the Client have a preferencial address?
+ How many times does this use case occur?
