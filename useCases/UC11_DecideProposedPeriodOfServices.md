# UC11 – Generate execution order

## Short format

-------------

The client initiates the decision of proposed periods for the requested services. The system shows the services and their respective proposed periods of execution, requesting confirmation. The client confirms. The system validates, **issues execution orders** and informs of the success of the operation.

## SSD

-------------

![SSD_UC11_LAPR.png](images/SSD_UC11_LAPR.png)

## Long format

-------------

### Main actor

Client

### Interested parts and their interests

- **Client:** intends that the requested services are executed in the periods that were indicated

- **Company:** wants that the services that the client requested is executed in the period that he requested

- **Service Provider:** wants to execute its services in the period the client requested

### Pre-conditions

(Client is logged in.)

### Post-conditions

Execution orders (one for each service) are emited.
Service request status is modified.

## Main success scenario (or basic flow)

-------------

1. The client initiates the decision of proposed periods for the requested services.
2. The system shows the services and their respective proposed periods of execution, requesting confirmation.
3. The client confirms.
4. The system validates, **issues execution orders** and informs of the success of the operation.

### Extensions (or alternative flows)

\*a. The Client concels the aproval process.

> The use case ends.

3a. Client rejects the servcices execution proposal.

> The decision is registered

> Service request status is modified

> The use case ends.

### Special requirements

\-

### List of Variations of Technologies and Data

\-

### Frequency of Occurrence

\-

### Open questions

- Can the client aprove services independently?
