# UC6 - Request for Service Provision

## Short Format

The customer initiates the service request. The system displays the postal addresses associated with the customer and prompts you to choose the address at which you want the services to be delivered. The customer selects the desired postal address. The system displays the service categories and asks the customer to select one. The client selects the category and the system presents the services of that category, asking the client to select one of them. The client selects the desired service, complements the request with a representative description of the task that is intended to be performed and, in the case of services that allow the duration specification, also indicates the expected duration of the task execution. The selection of categories and services described above is repeated until all the services intended by the client are specified. Subsequently, the system requests the introduction of one or more times (date and time of start) preferred for the execution of the task. The customer enters these times. The system validates the order, calculates its estimated cost and presents the result to the customer asking them to confirm. The customer confirms the request. The system registers it, assigns it a sequential number, **sends the request information by email to the customer** and presents it to the customer together with a message of success.

## SSD

![SSD_UC6_LAPR.png](images/SD_UC6_LAPR.png)

## Full Format

### Main actor

Client

### Parties and their interests

* **Client:** wants to register the request so that he can enjoy the services provided by the company.
* **Company:** wants customers to request services.

### Preconditions

* There is at least one service defined in the system.

### Post-conditions

The order information is stored in the system.

## Main success scenario (or base flow)

1. The customer initiates the service request.
2. The system shows the postal addresses associated with the customer and asks you to choose the address at which you want the services to be provided.
3. The customer selects the desired postal address.
4. The system displays the service categories and asks the customer to select one.
5. The customer selects the desired category.
6. The system presents the services of this category, asking the client to select one of them and complemented with a representative description of the task to be performed and, in the case of services that allow the specification of the duration, also informs the expected duration of the execution the task.
7. The customer selects the desired service and enters the estimated description and duration.
8. The system validates and stores the information entered.
9. Steps 4 through 8 are repeated until all services desired by the customer are specified.
10. The system prompts you to enter a preferred time (start date and time) to execute the task.
11. The customer enters the desired time.
12. The system validates and saves the time entered.
13. Steps 10 through 12 are repeated until at least one time is set.
14. The system validates the request, calculates the estimated cost, and presents the result to the customer asking them to confirm.
15. The customer confirms the request.
16. The system registers it, assigns it a sequential number, and presents it to the client together with a success message.

### Extensions (or Alternate Flows)

* \*a. **The client requests the cancellation of the registration.**
  * The use case ends.

* 2a. **The system detects that the customer only has a postal address.**
  1. The system assumes the known postal address and informs the customer thereof.
  2. The system advances to step 4.

* 3a. **The customer informs you that you want to use a different postal address.**
  1. The system allows the customer to associate another postal address with their information (UC7).
  2. The system returns to step 2.

* 6a. **There are no services specified for the category you want.**
  1. The system informs the customer of this fact.
  2. The system allows you to select another category (step 5).

* 8a. **Required minimum data missing.**
  1. The system informs you of missing data.
  2. The system allows the missing data to be entered (step 7)

* 12a. **Required minimum data missing.**
  1. The system informs you of missing data.
  2. The system allows you to enter the missing data (step 10)

* 14a. **The system detects that the entered data (or some subset of the data) is invalid.**
  1. The system alerts the customer to the fact.
  2. The system allows you to change it (step 3).
      * 2a. The client does not change the data. The use case ends.

### Special Requirements

* The minimum time for any service to be performed is 30 minutes, and it is only possible to request multiples of this value.
* The duration indication is limited by the selected (type of) service.

### List of Technologies and Data Variations

\ -

### Frequency of Occurrence

\ -

### Open questions

* Can the customer place an order without specifying a category?
* Is the description of the task to be performed mandatory?
* What is the maximum number of hours possible?
* Can a service be accepted without there being at least one service provider to perform the task?
* Should the system accept requests from all clients, or is there a list of banned / allowed clients (eg blacklist / whitelist of clients)?
* Should acceptance of a service request imply escalation of a service provider to the task?
* Knowing the cost is estimated, should we allow the client to specify a maximum value for the execution of the task?
