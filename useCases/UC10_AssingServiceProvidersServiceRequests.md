# UC10 – Assign Service Providers to Service Requests

## Short format

-------------
With the intent of the accomplishment of the services requests received, is needed to associate Services Providers to Service Requests, for this purpose, it is intended to be performed periodically through available scheduling systems like FIFO (first in, first out) and Random. The runtime of the algorithm must be configurable.
Distinct Services may have associated distinct Service Providers, is not guaranteed that il will be able to associate Service Providers to all Services.
The Service Provider that will be scheduled to do the service will be selected using, firstly, the Service Provider rating, secondly, the distance from the client and lastly alphabetically.

## SSD

-------------

![SSD_UC10_LAPR2.png](images/SSD_UC10_LAPR2.png)

## Long format

-------------

### Main actor

Company

### Interested parts and their interests

- **Client:** intends that the requested services are executed

-   **Company:** wants that the services that the client requested are associated to a Service Provider

-   **Service Provider:** wants to execute its services in the period the client requested

### Pre-conditions


### Post-conditions

## Main success scenario (or basic flow)
----------------------------------------------

1. The Timer starts the association for the services to Service Providers.
2. The System informs the success of the operation.  

### Extensions (or alternative flows)

a. The list of Services Requests is empty.
> The user case ends.

2b. The list of Services Providers is empty.
> The user case ends.

### Special requirements

\-

### List of Variations of Technologies and Data

\-

### Frequency of Occurrence

- The frequency of occurrence is the one defined in the configuration file in the project.

### Open questions

- Can the client accepts services independently?