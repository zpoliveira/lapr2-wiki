# UC5 - Specify Geographical Area

## Short Format

The administrator begins to specify a new geographical area. The system requests the required data (i.e. designation, **postal code, radius,** travel cost). The administrator enters the requested data. **The system obtains the postal codes covered by the new geographical area**, validates and presents the data to the administrator, asking him to confirm them. The administrator confirms. The system registers the new geographical area and informs the administrator of the success of the operation.

## SSD

![SSD_UC5_LAPR.png](images/SSD_UC5_LAPR.png)

## Long Format

### Main Actor

Administrator

### Parties and their interests

* **Administrator:** wants to specify geographic areas and associated travel costs.
* **Client:** wants to know in which geographical areas the company provides services and travel costs.
* **Company:** wants that the geographical areas are described in detail / detail.

### Pre-conditions

n/a

### Post-conditions

The geographical area is registred in the system.

## Main success scenario (or base flow)

1. The administrator begins to specify a new geographical area.
2. The system requests the required data (i.e. designation, **postal code, radius,** travel cost).
3. The administrator enters the requested data.
4. **The system obtains the postal codes covered by the new geographical area**, validates and presents the data to the administrator, asking him to confirm them.
5. The administrator confirms.
6. The system registers the new geographical area and informs the administrator of the success of the operation.

### Extensions (or alternate flows)

* **\*a. The administrator requests the cancellation of the geographic area specification.**
  1. The use case ends.

* **4a. Required minimum data missing.**
    1. The system tells which data is missing.
    2. The system allows to enter the missing data (step 3)
       * 2a. The administrator does not change the data. The use case ends.

* **4b. The system detects that the data (or some subset of the data) entered must be unique and already exist in the system.**
    1. The system alerts the administrator to the fact.
    2. The system allows to change it (step 3)
       * 2a. The administrator does not change the data. The use case ends.

* **4c. The system detects that the data entered (or some subset of the data) is invalid.**
    1. The system alerts the administrator to the fact.
    2. The system allows to change it (step 3)
       * 2a. The administrator does not change the data. The use case ends.

* **4d. The system can not determine the postal codes covered by the geographical area.**
    1. The system alerts the administrator to the fact.
       * 1a.  The use case ends.

### Special requirements

\-

### Technology and data variations list

\-
**O sistema deve recorrer a um serviço externo definido por configuração para obter os códigos postais abrangidos pela area geográfica**

### Occurrence frequence

\-

### Open questions

* Are there other data that are needed to identify the geographical area?
* What data is required to identify a geographical area?
* What data together allow the detection of duplication of geographical areas?
* Is it necessary to keep track of the cost of relocation associated with the geographical area?
* How often does this use case occur?
* **Should the system alert to territorial overlaps between geographic areas?**
* **Can postal codes obtained through the external service be modified (added, removed) by the administrator?**
