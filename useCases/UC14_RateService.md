# UC14 - Rate Service

## Short Format

The Client starts the rating of a executed service. The system shows all executed services and requests the selection of one. The Client selects service to rate. The system requests rating for the service. The client rates the service. The System shows the service and rating, requesting confirmation. The client confirms. The System diplays a success message.

## SSD

![SSD_UC14_LAPR.png](images/SSD_UC14_LAPR.png)

## Long Format

### Main actor

Client

### Parties and their interests

* **Client:** intends to rate the service that was executed, so that the quality of the SP can be visible.
* **Company:** intends that the clients rate all the services, so that the selection of SP can be easier.
* **Service Provider:** intends that their services can be rated, so that all clients can easily see how they work.

### Preconditions

At least a service has to be made to a client so that rating is possible

### Post-conditions

The rating is stored in the system.

## Main success scenario (or basic flow)

1. The Client starts the rating of a executed service.
2. The system shows all executed services and requests the selection of one.
3. The Client selects service to rate.
4. The system requests rating for the service.
5. The client rates the service.
6. The System shows the service and rating, requesting confirmation.
7. The client confirms.
8. The System diplays a success message.

### Extensions (or Alternate Flows)

* **\*a. The Client requests the cancellation of the rating process.**
  * The use case ends.

* **2a. No services to rate**
  1. The system informs the user about the inexistence of previous services.
        * 1a. The use case ends.

### Special Requirements

\ -

### List of Technologies and Data Variations

\ -

### Frequency of Occurrence

\ -

### Open questions
