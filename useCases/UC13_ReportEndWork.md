# UC13 – Report of End of The Work

## Short format

-------------

The Service Provider initiates the report of the end of the work. The system shows the execution Orders and their respective data that can be reported as ended. The Service Provier can select the different executions that have ended and add a issue if he whants. The Service Provider saves the inserted data. The system informs of the success of the operation.

## SSD

-------------

![SSD_UC13_LAPR.png](images/SSD_UC13_LAPR.png)

## Long format

-------------

### Main actor

Service Provider

### Interested parts and their interests

- **Service Provider:** intends to mark the execution orders as ended.

- **Company:** wants that the Services Providers mark the execution orders as ended.


### Pre-conditions

(Services Providers must be logged in.)

### Post-conditions


## Main success scenario (or basic flow)

-------------

1. The Service Provier initiates the report of the end of the work.
2. The system shows the execution Orders and their respective data that can be reported as ended.
3. The Service Provier can select the different executions that have ended and add a issue if he whants.
4. The Service Provider saves the inserted data.
5. The system informs of the success of the operation.

### Extensions (or alternative flows)

\*a. The Service Provier concels the report of the end of the work.

> The use case ends.

\*4.a. The Service Provider does not saves the inserted data.
> When leaving the system informs the possibility of non saved data.

### Special requirements

\-

### List of Variations of Technologies and Data

\- 

### Frequency of Occurrence

\-

### Open questions

