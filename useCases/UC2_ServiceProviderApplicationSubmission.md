# UC2 - Service Provider Application Submission

## Short Format

-------------

The unregistered user starts the submission of a new application. The systems requests the necessary data (full name, vat, phone number, email). The unregistered user inserts the requested data. The system shows the service categories and requests to select the ones to be done. The unregistered user selects the wanted ones. The system validates and presents the data, requesting to confirm. The unregistered user confirms the application data. The system regists the new application and informs the unregistered user that the operation succeded.

### SSD

![SSD_UC2_LAPR.png](images/SSD_UC2_LAPR.png)

## Long Format

-------------

### Main actor

Unregistered user

### Interested parts and its interests

**Unregistered user:** wants to present his application to be a service provider for the company.

**Company:** wants that any person who wants to colaborate with the company to be able to send an application.

### Pre-conditions

n/a

### Pos-conditions

The application to be a service provider is saved in the system.

### Main sucess scenario (or basic flow)

1. The unregistered user starts the application submission.

2. The system requests data (i.e. full name, vat, phone number, email).

3. The unregistered user inserts the requested data.

4. The system requests one postal address.

5. The unregistered user inserts the postal address.

6. The system validates and saves the inserted address.

7. Steps 4 and 6 are repeated until all the required postal addresses are entered (minimum 1).

8. The system requests an academic qualification.

9. The unregistered user inserts the academic qualification.

10. The system validates and saves the academic qualification.

11. Steps 8 to 10 are repeated until all academic qualifications have been entered.

12. The system requests a professional qualification.

13. The unregistered user inserts his professional qualification.

14. The system validates and saves the professional qualification.

15. Steps 12 to 14 are repeated until all professional qualifications have been entered.

16. The system requests the support documents.

17. The unregistered user inserts the support document.

18. The system validates and saves the support document.

19. Steps 16 to 18 are repeated until all support documents have been entered.

20. The system shows the service categories available in the system.

21. The unregistered user selects the service category he will realize.

22. The system validates and saves the selected category.

23. Steps 20 to 22 are repeated until all categories have been entered.

24. The system validates and shows the application data and asks for confirmation.

25. The unregistered user confirms the application data.

26. The system registers the new application and shows a success message.

### Extensões (ou fluxos alternativos)

+ **\*a. The unregistered user requests to cancel the application.**
    1. The use case ends.

+ **6a. Postal address data incomplete.**
    1. The system informs which is the missing data..
    2. The system allows an insertion of the missing data.(step 5)
        + 2a. The unregistered user doesn't modified the data. The use cases ends.

+ **10a. Academic qualification data incomplete.**
    1. The system informs which is the missing data.
    2. The system allows to inserts the missing data. (step 9)
        + 2a. The unregistered user doesn't modify the data. The use case ends.

+ **14a. Professional qualification data incomplete.**
    1. The system informs which is the missing data.
    2. The system allows to inserts the missing data. (step 13)
        + 2a. The unregistered user doesn't modify the data. The use case ends.

+ **14a. Document format its not supported.**
    1. The system informs which is the missing data.
    2. The system allows to insert other document. (step 13)
        + 2a. The unregistered user doesn't modify the data. The use case ends.

+ **20a. The system doesn't have service categories to show.**
    1. The system informs that there is no service categories.
        + The use case advances to step 24.

+ **24a. Minimal required data missing.**
    1. The system informs which is the missing data.
    2. The system allows to add the missing data. (step 3)
        + 2a. The unregistered user doesn't modify the data. The use case ends.

+ **24b. The system detects that the inserted data needs to be unique and they are already in the system.**
    1. The system alerts the unregistered user.
    2. The system allows alterations. (step 3)
        + 2a. The unregistered user doesn't modify the data. The use case ends.

+ **24c. The system detects that the inserted data are invalid.**
    1. The system alerts the unregistered user.
    2. The system allows to modify the data. (step 3).
        + 2a. The unregistered user doesn't modify the data. The use case ends.

### Special requests

n/a

### Tecnhology and data variations list

n/a

### Occurence frequence

n/a

### Open questions

+ Whats the required data for a valid application?

+ Whats the academic qualification necessary data?

+ Whats the professional qualification necessary data?

+ Whats the data that identify univocally an application?

+ How the unregistered user knows if his application was accepted?

+ There's a limit of service categories that the unregisted user can apply?

+ Who's responsible to turn the candidate into a service provider in the company?
