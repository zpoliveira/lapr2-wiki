# UC4 - Specify Service

## Short Format

The Administrator begins to specify a new service. The system requests the required data (i.e. **service type**, unique identifier, brief and complete description, category in which it is cataloged, and cost / hour). The administrator enters the requested data. The system validates and presents the data to the Administrator, asking you to confirm them. The Administrator confirms. The system records the data and informs the management of the success of the operation.

## SSD

![SSD_UC4_LAPR.png](images/SSD_UC4_LAPR.png)

## Long Format

### Main actor

Administrator

### Parties and their interests

* **Administrator:** intends to specify the services provided so that they can be requested by clients.
* **Client:** wants to know the services you can request.
* **Company:** wants the services described in detail / detail and well cataloged.

### Preconditions

n/a

### Post-conditions

Service information is logged in the system.

## Main success scenario (or base flow)

1. The Administrator starts the specification of a new service.
2. The system shows the types of services supported and asks to select one.
3. The Administrator selects the type of service you want.
4. The system requests the required data (i.e. unique identifier, brief and complete description and cost / hour).
5. The administrator enters the requested data.
6. The system shows the list of existing categories so that one is selected.
7. The Administrator selects the category in which you want to catalog the service.
8. The system requests additional data if the type of service warrants it.
9. The admin enters the requested data.
10. The system validates and presents the data to the Administrator, asking you to confirm them.
11. The Administrator confirms.
12. The system records the data and informs the management of the success of the operation.

### Extensions (or Alternate Flows)

* **\*a. The Administrator requests the service specification to be canceled.**
    * The use case ends.

* **2a. There are no service types defined in the system.**
    1. The system informs the administrator of this fact. The use case ends.

* **6a. There are no service categories defined in the system.**
    1. The system informs the Administrator of this fact.
    2. The system allows the creation of a new category (UC 3).
        * 2a. Administrator does not create a category. The use case ends.

* **8a. No further data required.**
    1. The system proceeds immediately to step 10.

* **10a. Required minimum data missing.**
    1. The system informs you of missing data.
    2. The system allows the missing data to be entered (step 3)
        * 2a. The Administrator does not change the data. The use case ends.

* **10b. The system detects that the data (or some subset of the data) entered must be unique and already exist in the system.**
    1. The system alerts the Administrator to the fact.
    2. The system allows you to change it (step 3)
        * 2a. The Administrator does not change the data. The use case ends.

* **10c. The system detects that the entered data (or some subset of the data) is invalid.**
    1. The system alerts the Administrator to the fact.
    2. The system allows you to change it (step 3).
        * 2a. The Administrator does not change the data. The use case ends.

### Special Requirements

\ -

### List of Technologies and Data Variations

\ -

### Frequency of Occurrence

\ -

### Open questions

* Are all data required for the specification of a service?
* Can I specify a service with no associated category?
* Can a service belong to more than one category?
* What data together allow you to detect duplicate services?
* Is it necessary to save the change history of a service?
* How often does this use case occur?
