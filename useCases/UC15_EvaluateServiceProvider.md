# UC15 - Evaluate Service Provider

## Brief format

The human resources officer (hro) starts the evaluation process. The system shows all the service providers and asks to select one. The hro selects the service provider. The system requests to insert the rating. The hro inserts the rating. The system shows the service provider and the choosen rating and asks for confirmation. The hro confirms and the system shows the success of the operation.

## SSD

![SSD_UC15_LAPR.PNG](images/SSD_UC15_LAPR.PNG)

## Full Format

### Principal actor

Human Resources Officer

### Interested parts and its interests

* **Human Resources:** Wants to avaliate the service providers that work for the company for a clear image of the service provider.
* **Client:** Wants to know how good the service provider is.
* **Service Provider:** Wants to have is rating well done.

### Pre-conditions

n/a

### Pos-conditions

The rating of the service provider is registed in the system.

## Main success scenario (or basic flow)

1. The human resources officer starts the evaluation process.
2. The system shows the service providers and asks to select one.
3. The hro selects the service provider he wants to evaluate.
4. The system requests the rating of the service provider.
5. The hro inserts the rating.
6. The system asks for confirmation.
7. The hro confirms.
8. The system saves the rating and shows the success of the operation.

### Extensions (or alternative flow)

*a. The hro requests to cancel the evaluation process.

* The use case ends.

**2a. There is no service providers in the system.**

  1. **The system shows an empty list. The use case ends.**

### Special requests

\-

### Technology and data variation list

\-

### Occurrence rate

\-

### Open questions

* The rating is only a number or has more data like description?
* Whats the frequency of this use case?
