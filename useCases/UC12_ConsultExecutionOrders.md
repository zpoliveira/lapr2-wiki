# UC12 – Consult Execution Orders

## Short format

-------------

The Service Provider initiates the query of the execution orders that he has been assingn. The system shows the execution Orders and their respective data. The Service Provier can do an export of this data to different file formats ("CSV", "XML", "XLS"). The system informs of the success of the operation.

## SSD

-------------

![SSD_UC12_LAPR.png](images/SSD_UC12_LAPR.png)

## Long format

-------------

### Main actor

Service Provider

### Interested parts and their interests

- **Service Provider:** intends to see the execution orders that are assign to him to execute its services in the period the client requested

- **Company:** wants that the Services Providers know the execution orders that the have been assign


### Pre-conditions

(Services Providers must be logged in.)

### Post-conditions


## Main success scenario (or basic flow)

-------------

1. The Service Provier initiates the query of the execution orders that he has been assingn.
2. The system shows the execution Orders and their respective data.
3. The Service Provier can export this data to a file.
4. The system informs of the success of the operation.

### Extensions (or alternative flows)

\*a. The Service Provier concels the query.

> The use case ends.

### Special requirements

\-

### List of Variations of Technologies and Data

\- Data can be exported in different file formats

### Frequency of Occurrence

\-

### Open questions

