# UC8 - Register Service Provider

## Short format

The HRO starts registration of the new Service Provider. The system requests the SP VAT number.  The HRO enters the requested data. The system validates and displays the data, asking confirmation. The HRO confirms the data presented by the system. The system **registers the data of the new Service Provider** and informs the HRO of the success of the operation. The system **sends the access data to the new Service Provider**.

## SSD

![UC8-SSD-LAPR.png](images/SSD_UC8_LAPR.png)

## Long format

### Main actor

Human Resources Officer (HRO)

### Interested parts and their interests

* **HRO** intends to register the Service Providers so that it's services are made available to the company.
* **Company:** intends that the Service Provider becomes available to perform the services requested by its clients.
* **Service provider** needs to have an access profile to indicate its availability in order to perform the services requested by the Company's customers.

### Pre-conditions

There are Service Categories and Geographic Areas defined in the system.

### Post-conditions

The registration information is stored in the system.

## Main success scenario (or basic flow)

1. The HRO starts registering a new Service Provider.
2. The system requests the SP VAT number.
3. The HRO inserts the data.
4. The system validates and displays the data, requesting confirmation.
5. The HRO confirms.
6. The system **registers the data of the new Service Provider, sends the access data to the new Service Provider** and informs the HRO of the success of the operation.

### Extensions (or alternative flows)

*a. The HRO requests the cancellation of the registration.

> The use case ends.

2a. Inexistent VAT number
> 1. The system informs the HRO about the inexistince of that VAT number
> 2. The systems allows the introduction of new data (postal code, ID number, full and abbreviated name, institutional email)
>> 2a. The HRO does not change the data. The use case ends.

5a. The HRO does not confirm the data
> 1.The systems allows the introduction of new data (postal code, ID number, full and abbreviated name, institutional email)
>> 1a. The HRO does not change the data. The use case ends.

5b. Incomplete / duplicate Category data.
> 1. The system informs which data is missing or duplicate
> 2. The system allows the introduction of new data
>> 2a. The HRO does not change the data. The use case ends.

5c. Incomplete / duplicate Geographic Area data.
> 1. The system informs which data is missing or duplicate
> 2. The system allows the introduction of new data
>> 2a. The HRO does not change the data. The use case ends.

5d. The system detects that the entered data (or some subset of the data) is invalid.
> 1. The system alerts the HRO to the fact.
> 2. The system allows you to change it.
>> 2a. The HRO does not change the data. The use case ends.

### Special requirements

\-

### List of Variations of Technologies and Data

\-

### Frequency of Occurrence

\-

### Open questions

* Are there any other mandatory data besides those already known?
* What data together allow you to detect the duplication of Service Providers?
* What are the security rules applicable to access data?
* What is the email system to adopt?
* Do we have to support more than one email system?
* How is defined which email system to use?
* How often does this use case occur?
