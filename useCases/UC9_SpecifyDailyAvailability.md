# UC9 - Indicate Daily Availability

## Short Format

The service provider starts identifying his/her daily availability. The system requests the required data (i.e. start/end date, start/end time). The service provider enters the requested data. The system validates and presents the data to the service provider for confirmation. The service provider confirms. The system records the availability of the service provider and informs the service provider of the success of the operation.

## SSD

![SSD_UC9_LAPR.png](images/SSD_UC9_LAPR.png)

## Full Format

### Main actor

Service Provider

### Parties and their interests

* **Service provider:** is intended to indicate the daily availability to perform services.
* **Company:** wants the daily availability of service providers to be specified in the system.

### Preconditions

n/a

### Post-conditions

Information about the daily availability (s) of the service provider is recorded in the system.

## Main success scenario (or base flow)

1. The service provider initiates the identification of their daily availability.
2. The system requests a period (starting date and time and end date and time) in which the service provider is available to perform services.
3. The service provider enters the requested data.
4. The system validates and displays the data for confirmation.
5. The service provider confirms.
6. The system records the availability period of the service provider and informs the service provider of the success of the operation.
7. Steps 2 to 6 shall be repeated until the service provider has indicated all his availabilities.

### Extensions (or Alternate Flows)

* **\*a. The service provider requests the cancellation of the hourly availability indication.**
  * The use case ends.

* **4a. Data that identifies the availability period is incomplete.**
    1. The system informs you of missing data.
    2. The system allows the missing data to be entered (step 3)
        * 2a. The service provider does not change the data. The use case ends.

* **4b. The system detects that the entered data (or some subset of the data) is invalid.**
    1. The system alerts the service provider to the fact.
    2. The system allows you to change it (step 3).
        * 2a. The service provider does not change the data. The use case ends.

* **4c. The system detects that the indicated availability overlaps (i.e., intercept) with another availability indicated above.**
    1. The system alerts the service provider to the fact.
    2. The system allows you to change it (step 3)
        * 2a. The service provider does not change the data. The use case ends.

### Special Requirements

\ -

### List of Technologies and Data Variations

\ -

### Frequency of Occurrence

\ -

### Open questions

* Should the service provider indicate the daily availability for each type of service?
* How often does this use case occur?
