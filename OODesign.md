# Use Cases Realization (SD + CD)

| UC  |Description|
|:----|:------------------------------------------------------------------------|
| UC1 | [Client Registration](Design/UC1_ClienteRegistration.md)   |
| UC2 | [Service Provider Application Submission](Design/UC2_ServiceProviderApplicationSubmission.md)  |
| UC3 | [Specify Category](Design/UC3_SpecifyCategory.md) |
| UC4 | [Specify Service](Design/UC4_SpecifyService.md) |
| UC5 | [Specify Geographical Area](Design/UC5_SpecifyGeographicalArea.md) |
| UC6 | [Service Request](Design/UC6_RequestServiceRequest.md) |
| UC7 | [Associate Postal Address](Design/UC7_AssociatePostalAddress.md) |
| UC8 | [Register Service Provider](Design/UC8_RegisterServiceProvider.md) |
| UC9 | [Specify Daily Availability](Design/UC9_SpecifyDailyAvailability.md) |
| UC10 | [Assing Service Providers to a Service Request](Design/UC10_AssingServiceProvidersServiceRequests.md) |
| UC11 | [Decide Proposed Service Schedules](Design/UC11_DecideProposedPeriodOfServices.md) |
| UC12 | [Consult Execution Orders](Design/UC12_ConsultExecutionOrders.md) |
| UC13 | [Report End of Work](Design/UC13_ReportEndWork.md) |
| UC14 | [Rate Service](Design/UC14_RateService.md) |
| UC15 | [Evaluate Service Provider](Design/UC15_EvaluateServiceProvider.md) |
