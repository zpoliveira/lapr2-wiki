# Supplementary Specification

## Functionalities

*It specifies the features that do not relate to the use cases, namely: Audit, Report and Security.*

- **Security/Authentication**:
- **Audit**:

## Usability

*Evaluates the user interface. It has several subcategories,
among them: error prevention; aesthetics and design; help and
documentation; consistency and standards.*

## Reliability

*Refers to software integrity, compliance, and interoperability. The requirements to be considered are: frequency and severity of failure, possibility of recovery, possibility of forecasting, accuracy, mean time between failures.*

## Performance

*It evaluates the software performance requirements, namely: response time, memory consumption, CPU utilization, load capacity and application availability.*

## Sustainability

*The supportability requirements group several features, such as:
testability, adaptability, manutiability, compatibility,
configurability, installability, scalability and more.*

## +

### Design restrictions

*Specifies or restricts the system design process. Examples may include: programming languages, software process, use of development tools, class library, etc.*

### Implementation restrictions

*Specifies or restricts the code or construction of such a system
such as: mandatory standards, implementation languages,
database integrity, resource limits, operating system.*

### Interface restrictions

*Specifies or restricts the functionality inherent in the with the user.*

### Physical restrictions

*Specifies a limitation or physical requirement of the hardware used, for material, shape, size or weight.*
