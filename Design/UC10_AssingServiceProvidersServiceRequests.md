# Realization of UC10 Assign Service Providers to Service Requests

## Rationale

| Main flow | Answer | Justification |
|:-------------------------------------------------------------------------------------------------------|:------------------------------------------------------------|:-----------------------------------------------|:---------------------------------------------------------------------------------------------------------------------|
|1. Timer starts the assign of Service Providers to the services in the Service Requests.|... creates /instance the task to be executed?|Company|IE, Company knows delay, interval with witch the task with be executed|
||... is responsible for associate SP to Services?|associateServiceProviderTask|IE, knows its data|
|| ...knows the registers to fetch data | Company |IE, knows its data|


## Systematization

 From the rational, the conceptual classes promoted to software classes are:


* Company
* Client
* Execution Orders
* Service Request
* Service Order

Other software classes (i.e. Pure Fabrication) also identified:

* DecideProposedPeriodOfServicesUI  
* DecideProposedPeriodOfServicesController
* ExecutionOrderRegistry
* ServiceOrderRegitry
* ServiceRequestRegistry


## Sequence Diagram

![SD_UC10_LAPR.png](images/SD_UC10_LAPR.png)

![SD_UC10_LAPR_PT2.png](images/SD_UC10_LAPR_PT2.png)

![SD_UC10_LAPR_PT3.png](images/SD_UC10_LAPR_PT3.png)

## Class Diagram

![CD_UC10.png](images/CD_UC10.png)