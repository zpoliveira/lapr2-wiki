# Realization of UC4 Rate Service

## Rationale

| Main flow | Question: Which Class... | Answer | Justification |
|:-------------------------------------------------------------------------------------------------------|:------------------------------------------------------------|:-----------------------------------------------|:---------------------------------------------------------------------------------------------------------------------|
|1. The Client starts service rating process.|... interacts with the HRO?|RateServiceUI|Pure Fabrication UI|
||... coordinates the UC?|RateServiceController|Pure Fabrication Controller|
|2. The system shows all services and requests selection of one.| | | |
|3. The Client makes the selection| | ||
|4. The system requests rating.||||
|5. The Client inserts rating ||||
|6. The system shows the service and the rating and requests confirmation.|||
|7. The Client confirms ||||
|8. The Client inserts rating |... saves the rating of this Service?| Service Evaluation| IE: Created in step 1 |
|| ... validate the rating (local validation)?  | ServiceEvaluation| IE: ServiceEvaluation has its own data|
|| ... validate the rating (global validation)? | ServiceEvaluationRegister| IE: ServiceEvaluationRegister contains / aggregates all serviceEvaluation.|

## Systematization

 From the rationale, the conceptual classes promoted to software classes are:

* Company
* Client
* ClientRegister
* ServiceEvaluation

Other software classes (i.e. Pure Fabrication) also identified:

* RateServiceUI  
* RateServcieController
* ClientRegister
* ExecutionOrdersRegister
* ServiceEvaluationregister

## Sequence Diagram

![SD_UC14_LAPR.png](images/SD_UC14_LAPR.png)

## Class Diagram

![CD_UC14_LAPR.png](images/CD_UC14_LAPR.png)
