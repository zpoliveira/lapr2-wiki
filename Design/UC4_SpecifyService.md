# Performing UC4 Specify Service

## Rationale

| Home | Question: What Class ... | Reply | Justification |
|:----- |:-------------- |:--------- |:------|
| 1. The administrator starts specifying a new service. |... interact with the user? | SpecifyServiceUI  | PureFabrication |
|| ... coordinates the UC? | SpecifyServiceController | Controller |
|| ... create / service instance? | ServiceType | By the application of Creator (R1) it should be the Company. But, by application of HC + LC, the Company would delegate this responsibility to ServiceRegister. In addition, the "service" to be created depends on its type. However, who knows the class corresponding to the type of service to be created is "Service Type". |
| 2. The system shows the types of services supported and asks you to select one. | ... do you know the types of services supported? | ServiceType | IE: in MD the Company has ServiceType. By application of HC + LC delegates in ServiceTypeRegister |
| 3. The administrator selects the type of service you want. | ... save the selected service type? | Service | IE. The service is of a type - created service instance.
| 4. The system requests the required data (i.e. unique identifier, brief and complete description and cost / hour). | | | |
| 5. The administrator enters the requested data. | ... save the entered data? | Service | Information Expert (IE) - Instance Created Previously |
| 6. The system shows the list of existing categories so that one is selected. | ... knows the existing categories to list? | CategoryRegister | IE: Company has / aggregates all Category and by application of HC + LC delegates in CategoryRegister |
| 7. The administrator selects the category in which you want to catalog the service. | ... save the selected category? | Service | IE: Categorized service in a Category - previously created instance |
| 8. The system requests additional data if the type of service justifies it. | ... do you know if more data is needed? | Service | IE: previously created instance |
| 9. The administrator enters the requested data. | ... save the additional data? | Service | IE: previously created instance |
| 10. The system validates and submits the data to the administrator requesting confirmation. | ... validate the Service data (local validation)? | Service | IE: Service has its own data |
| | ... validate the Service data (global validation)? | Company | IE: The Company contains / aggregates Services and delegates to RegistryServices (HC + LC) ||
| 11. The administrator confirms. | | |
| 12. The system records the data and informs the management of the success of the operation. | ... save the specified / created service? | Company | IE. In the MD the Company contains / aggregates Services and delegates in Services Registry (HC + LC) |
|| ... notify the user? Specify ServiceUI ||

## Systematization

 From the rationale it results that the conceptual classes promoted to classes of software are:

* Company
* Service Type
* Service
* Category

Other software classes (i.e. Pure Fabrication) identified:

* Specify ServiceUI
* Specify ServiceController
* ServiceTypeRegister
* Service Registration
* RegistrationCategories

## Sequence Diagram

![SD_UC4_LAPR.png](images/SD_UC4_LAPR.png)

## Class Diagram

![CD_UC4_LAPR.png](images/CD_UC4_LAPR.png)
