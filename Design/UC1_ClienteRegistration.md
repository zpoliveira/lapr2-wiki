# Realization of UC1 Register as Client

## Rationale

| Main Flow|  Question: What Class ...|  Answer|  Justification|
|----------------|----------|------------------|------------------|
|  1. The unregistered user starts the registration as a client.|  ... interacts with the user?|  RegisterClientUI|  PureFabrication|
||  ... coordinates the UC?|  RegisterClientController|  Controller|
||  ... create / instance Client?| ClientRegister| HC + LC (on Company) + Creator (Rule 1)|
||  ... who knows the Customer Register?| Company| HC + LC|
|  2. The system requests the required data (i.e. full name of person, NIF, telephone contact, email and password).||||
|  3. The unregistered user enters the requested data. ... Save the entered data?|  Client|  Information Expert (IE) - instance created in step 1||
|  4. The system requests a postal address.||||
|  5. The unregistered user enters the postal address.|  ... create / instance Postal Address?|  Creator (Rule 4)||
||  ... save the entered data? Information Expert (IE) - instance created in this step  | PostalAddress|  IE: a Postal Address has a Postal Code|
|  6. The system validates and stores the entered address.|  ... save the created Postal Address instance? Client|  Information Expert (IE) - In MD the Client mentions one or more Postal Address  ||
| 7. Steps 4 to 6 are repeated until all the required postal addresses are entered (minimum 1).||||
|8. The system validates and presents the data, asking you to confirm them.|... validates the data of the Client (local validation)|Client|E: Client has its own data|
||  ... validates the customer data (global validation)| ClientRegister|  IE: The ClientRegister contains/agregates Clients|
|  9. The unregistered user confirms.||||
|  10. system registers the customer and registered user data and informs the unregistered user of the success of the operation. the customer record contains / adds Services  ||||
||  ... user guardian of this client?|  Authorization|  IE. User management is the responsibility of the respective external component whose point of interaction is through the class AuthorizationFacade  |
||  ... notify the user?|  RegisterClientUI||

## Sistematization

From the rationale results that the conceptual classes promoted to classes of software are:

* Company
* Postal Address
* Postal Code
* Client
* ClientRegister

Other software classes (i.e. Pure Fabrication) identified:

  * RegisterClientUI
  * RegisterClientController

**Note:** The responsibility of creating instances of Postal Address to the Customer has been assigned.
However, a customer is only valid when he has at least one Postal Address.
That is, to exist a Customer it is necessary to have a Postal Address.
Thus, it is not feasible to ask a Client instance to create a Postal Address.
To resolve this issue the creation of instances of Postal Address is done through a static method in the Client class. Therefore, it is not necessary to have a client instance previously. Other approaches / alternatives are possible.

## Sequence Diagram

![SD_UC1_LAPR.png](images/SD_UC1_LAPR.png)

## Class Diagram

![CD_UC1_LAPR.png](images/CD_UC1_LAPR.png)
