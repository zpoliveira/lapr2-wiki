# Realization of UC7 Associate postal address

## Rationale

--------

| Main flow                                                               | Question: Which Class...                             | Answer                         | Justification                                                                                            |
|-------------------------------------------------------------------------------|----------------------------------------------------|----------------------------------|---------------------------------------------------------------------------------------------------------|
| 1. The client initiates the association of a new postal address with their information.                     | ... interacts with the user?                     | AssociatePostalAdressUI         | PureFabrication, as there is no justification for assigning this responsibility to any existing class in the MD. |
|                                                                               | ... coordinates the UC?                                  | AssociatePostalAdressController | Controller                                                                                              |
| 2. The system requests the required data (i.e. postal address).            | n/a                                                |                                  |                                                                                                         |
| 3. The Customer enters the requested data.                                   | ... create / instantiate postal addresses?             | Client                          | Creator (rule 4)                                                                                       |
|                                                                               | ... save the data entered?                  | postalAddress                   | IE - instance created in step 1                                                                        |
|||postalAddress|IE. a Postal Address has a postalCode|
| 4. The system validates and displays the data, asking you to confirm them.            | ... validate the Address data (local validation)?  | Postal Address                   | IE: postalAdress has its own data                                                        |
|                                                                               | ... validate the address data (global validation)? | Client                          | IE: Client contains / aggregates all your postal addresses.                                                           |
| 5. Client confirms.                                                        |                                                    |                                  |                                                                                                         |
| 6. The system **associates the postal address with the customer** and informs of the success of the operation. | ... save the created Postal Address?                       | Client                  | IE: Client contains / aggregates all your postal addresses.                                      |
|                                                                               | ... notify the user?                         | AssociatePostalAdressUI         |                                                                                                         |

## Systematization

From the rationale, the conceptual classes promoted to software classes are:

- Company

- Client

- Postal Address

- Postal Code

Other software classes (i.e. Pure Fabrication) also identified:

- AssociatePostalAdressUI

- AssociatePostalAdressController

## Sequence Diagram

--------

![SD_UC7_LAPR.png](images/SD_UC7_LAPR.png)

**Note:** It was not considered relevant to promote (by application of HC + LC) the list of Postal Addresses of a client to a specific class of software. However, this could have been done and would be perfectly valid.

## Classes Diagram

--------

![CD_UC7_LAPR.png](images/CD_UC7_LAPR.png)
