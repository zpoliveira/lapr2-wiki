# Realization of UC8 Service Provider register

## Rationale

| Main flow| Answer| Justification |
|:-------------------------------------------------------------------------------------------------------|:------------------------------------------------------------|:-----------------------------------------------|:---------------------------------------------------------------------------------------------------------------------|
|1. The HRO starts registering a Service Provider.|... interacts with the HRO?|ServiceProviderRegistryUI|Pure Fabrication UI|
||... coordinates the UC?|ServiceProviderRegistryController|Pure Fabrication Controller|
||create / instance Service Provider?|ServiceproviderRegistry| Pattern HC + LC (on Company) + Creator (Rule 1)|
|2. The system requests the required data (i.e. VAT number).| | | |
|3. The HRO enters the requested data| ... saves the inserted data? |Service Provider |Information Expert (IE) - instance created in step 1|
|4. The system validates and displays the data, requesting that you confirm them.|... validates the data of the Service Provider (local validation)?|Service Provider|IE: Service Provider has its own data|
||...Validates Service Provider data (global validation)?|ServiceProviderRegistry|IE: ServiceProviderRegistry Contains / Adds Service Providers|
|5. The HRO confirms. ||||
|6. The system **registers the registered Service Provider and Service Provider data, notifies the Service Provider of their access data** and informs the HRO of the success of the operation.|...saves the registered Service Provider?|ServiceProviderRegistry|IE: ServiceProviderRegistry contains / aggregates Service Providers|
|| ... save the user data of this Service Provider?  | AuthorizationFacade | IE. User management is the responsibility of the respective external component whose point of interaction is through the class "AuthorizationFacade" |
|| ... notifies the Service Provider of their access data  | ServiceProviderRegistry | IE: has the data to do this.|

## Systematization

From the rationale, the conceptual classes promoted to software classes are:

* Company
* HRO
* Service provider
* Category **(if the VAT Number doesn't exist or the HRO doesn't validate the data)**
* geographic Area **(if the VAT Number doesn't exist or the HRO doesn't validate the data)**

Other software classes (i.e. Pure Fabrication) also identified:

* ServiceProviderRegistryUI  
* ServiceProviderRegistryController
* ServiceProviderRegistry
* CategoryRegistry
* GeographicAreaRegistry
* Authorization/Facade

## Sequence Diagram

![SD_UC8_LAPR.png](images/SD_UC8_LAPR.png)

**Note:** The Service Provider has two lists (SPCategoriesList and SPGeographicAreasList) obtained by application of HC + LC. However, it was decided not to expose these lists abroad. That is, the delegation of responsibilities that occurred is not known from the rest of the system (i.e. private delegation of responsibilities).

## Class Diagram

![CD_UC8_LAPR.png](images/CD_UC8_LAPR.png)
