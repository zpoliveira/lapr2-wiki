# Realization of UC3 Specify Category

## Rationale

| Home | Question: What Class ... | Reply | Justification |
|:-----|:---------|:------ |:-------------|
| 1. The administrator begins to specify a new category. | ... interact with the user? | SpecifyCategoryUI | Pure Fabrication, since it is not justified to assign this responsibility to any class exists in the Domain Model. |
|| ... coordinates the UC? |SpecifyCategoryController | Controller. |
|| ... creates / instantiates Category? | Company | Creator (Rule 1) |
| 2. The system requests the required data (i.e. unique code and description). ||||
| 3. The administrator enters the requested data. | ... saves the entered data?| Category | Information Expert (IE) - instance created in step 1 |
| 4. The system validates and presents the data to the administrator, asking them to confirm them.| ...validates the Category data (local validation)? | Category | IE: Category has its own data |
|| ...validates Category data (global validation)? | Company | IE: The Company contains / aggregates Categories |
| 5. The administrator confirms. |||
| 6. The system records the data and informs the administrator of the success of the operation. | ...saves the Category specified / created? |  Company | IE. In MD the Company contains / aggregates Categories |
|| ... notify the user? | SpecifyCategoryUI ||

## Systematization

 From the rationale it results that the conceptual classes promoted to classes of software are:

* Company
* Category

Other software classes (i.e. Pure Fabrication) identified:

* SpecifyCategoryUI
* SpecifyCategoryController

## Sequence Diagram

![SD_UC3_LAPR.png](images/SD_UC3_LAPR.png)

## Class Diagram

![CD_UC3_LAPR.png](images/CD_UC3_LAPR.png)
