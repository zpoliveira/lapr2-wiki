# Realization of UC6 Requests for Service Execution

## Rationale

| Home | Question: What Class ... | Reply | Justification |
|:---|:---|:---|:---|
| 1. The customer initiates the service request. | ... interact with the user? | ServiceRequestExecutionUI | Pure Fabrication, since it is not justified to assign this responsibility to any class exists in the Domain Model. |
|| ... coordinates the UC? | ServiceRequestExecutionController | Controller | |
|| ... create / request ServiceRequest? | ServiceRequestRegister | Creator (rule 1) + HC + LC on Company | |
| 2. The system displays the postal addresses associated with the customer and prompts you to choose the address at which you want the services to be delivered. | ...knows the customer's postal addresses? | Customer | IE: Client has 1 or more Addresses |
| 3. The customer selects the desired postal address. | ... saves the selected postal address? | ServiceRequest | IE: Instance created previously. In the MD a ServiceRequest is made in a PostalAddress. |
| 4. The system displays the service categories and asks the customer to select one. | ​​... do you know the existing categories? | CategoryRegister | IE: CategoryRegister has Category |
| 5. The customer selects the desired category. | | | |
| 6. The system presents the services of this category, asking the client to select one of them and complemented by a representative description of the task to be performed and, in the case of services that allow the specification of the duration, also informs the duration of the task . | ... do you know the services of a given category? | Company | IE: ServiceRegister knows all Services. |
||| Service | IE: In the MD you know the Category in which it is cataloged.
| 7. The customer selects the desired service and enters the description and estimated duration. | |||
| 8. The system validates and stores the information entered. In the MD a Request has several Service Description Ordered with this data.
|| ... create / instance OrderService? | ServiceRequest | Creator (rule 1) |
|| ... validates the data? (local) | Description: | OrderService | IE: are your own data.
|| ... validates the data? (global) | IE: knows all of its descriptions.
| 9. Steps 4 through 8 are repeated until all services desired by the customer are specified. ||||
| 10. The system prompts you to enter a preferred time (start date and time) to execute the task. ||||
| 11. The customer enters the desired time. ||||
| 12. The system validates and saves the entered time. | ... validate and save the indicated time? | SchedulePreference | IE: are the data of this concept.
||| ServiceRequest | IE: in the MD a ServiceRequest has several SchedulePreference |
|| ... create / instantiate PreferenceTime? | ServiceRequest | Creator (rule 1) |
| 13. Steps 10 through 12 are repeated until at least one time is set. | | |
| 14. The system validates the request, calculates the estimated cost, and presents the result to the customer asking them to confirm. | ...validates the request (local validation) | ServiceRequest | IE: know your own information.
|| ... validates the request? (global validation) | ServiceRequestRegister | IE: ServiceRequestRegister knows all the requests |
|| ... calculate the total cost? | ServiceRequest | IE: knows all the services requested and the postal address where they will be provided.
|||ServiceRequestSpecification| IE: knows the duration and the service you want |
|||Service| IE: Know your cost / hour |
|| ... save the cost of travel? | OtherCost | IE: in MD an Order has several OtherCost |
|| ... create / instance | OtherCost | Creator (rule 1) |
| 15. The customer confirms the request. |||
| 16. The system registers it, assigns it a sequential number and presents it to the customer together with a success message. | ... generate the sequential number? | Registration | Service Request | IE: know all the requests already received.
|| ... save the sequential number? | ServiceRequestService | IE: Instance created previously |
|| ... save the Order? | RegistrationServiceService | IE: RegistrationServiceService receives all Orders |
|| ... notifies the generated number and the success information? | RequestPrivacyServiceUI ||

## Systematization

 From the rationale it results that the conceptual classes promoted to classes of software are:

* Company
* Client
* Service
* Category
* ServiceRequest
* ServiceRequestSpecification
* SchedulePreference
* OtherCost

Other software classes (i.e. Pure Fabrication) identified:

* ServiceRequestExecutionUI
* ServiceRequestExecutionController
* ClientRegister
* CategoryRegister
* ServiceRegister

## Sequence Diagram

![SD_UC6_LAPR.png](images/SD_UC6_LAPR.png)

![SD_UC6_Calculo.png](images/SD_UC6_Calculo.png)

## Class Diagram

![CD_UC6_LAPR.png](images/CD_UC6_LAPR.png)
