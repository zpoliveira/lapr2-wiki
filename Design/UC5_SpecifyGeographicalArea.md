# UC5 Specify Geographical Area

## Rationale

| Main Flow                                                                                        | Question: What class...                                      | Response                                       | Justification                                                                                                         |
|:-------------------------------------------------------------------------------------------------------|:------------------------------------------------------------|:-----------------------------------------------|:---------------------------------------------------------------------------------------------------------------------|
| 1. The administrator begins to specify a new geographical area. | ... interacts with the user? | SpecifyGeographicalAreaUI.                          | Pure Fabrication, because it is not justified to attribute this responsibility to any class exists in the Domain Model. |
|| ... coordinates the UC?                                                                              | SpecifyGeographicalAreaController                                | Controller.                                    |                                                                                                                      |
|| ... creates the geographical area?          | GeographicalAreaRegister                                               | Creator (Rule 1) + HC+LC : Company delegates to GeographicalAreaRegister                          |                                                                                                                      |
| 2. The system requests the required data (i.e. designation, **postal code, radius,** travel cost).  |                  |                                                |                                                                                                                      |
| 3. The administrator enters the requested data.   | ... saves the inserted data?                    |GeographicalArea                                    | Information Expert (IE) - instance created in step 1                                                                                              |
| 4. **The system obtains the postal codes covered by the new geographical area**, validates and presents the data to the administrator, asking him to confirm them.                                                             | ... validates the geographical area data? (local validation) | GeographicalArea                                     | IE: GeographicalArea has its own data.                                                                                                                   |
|| ... validates the geographical area data? (global validation)                                          | GeographicalAreaRegister                                               | IE: GeographicalAreaRegister has/aggregates GeographicalArea |                                                                                                                      |
||...makes postal codes available within a radius.|ExternalService|IE: In the DM ExternalService has this information. |
||...whats the expected result of the ExternalService| List\<ActsIn>|IE: In the DM ExternalService informs multiple "ActsIn". |
||... knows the ExternalService.|Company|IE: In the DM the Company defines ExternalService. Protected Variation over ExternalService since the system must support several external services|
||...implements the particularities of each external service in concrete?|ExternalServiceXXXAdapter|ProtectedVariation + Adapter|
| 5. The administrator confirms.||||
| 6. The system registers the new geographical area and informs the administrator of the success of the operation.                           | ... saves the geographical area created?| Company| IE. In the DM the Company acts in multiple geographical areas.|
|| ...notifies the user?| SpecifyGeographicalAreaUI                                        |                                                |                                                                                                                      |

## Systematization

From the rationale it results that the conceptual classes promoted to classes of software are:

* Company
* Geographical Area
* ExternalService
* ActsIn

Other software classes (i.e. Pure Fabrication) identified:  

* SpecifyGeographicalAreaUI  
* SpecifyGeographicalAreaController
* GeographicalAreaRegister

## Sequence Diagram

### Base solution

![SD_UC5_LAPR.png](images/SD_UC5_LAPR.png)

**This base solution is complemented / detailed with one of the alternatives presented below.
"Interaction Use" was applied so that the Sequence Diagram refers to the diagram that details / complements it.**

![SD_UC5_LAPR_Detail.png](images/SD_UC5_LAPR_Detail.png)

## Class Diagram

![CD_UC5_LAPR.png](images/CD_UC5_LAPR.png)
