# Realization of UC15 Evaluate Service Provider

## Rationale

| Main Flow| Question: What class...| Response| Justification|
|:-------------------------------------------------------------------------------------------------------|:------------------------------------------------------------|:-----------------------------------------------|:---------------------------------------------------------------------------------------------------------------------|
|1. The HRO starts the evaluation process.|... interacts with the HRO?|EvaluateSPUI|PureFabrication UI|
||...coordinates the UC?|EvaluateSPController|PureFabrication Controller|
|2. The system shows all the service providers and asks to select one.| ... knows the sevice providers?|ServiceProviderRegistry | Information Expert (IE) |
|3. The HRO selects the service provider.||||
|4. The system requests to make the evaluation.||||
|5. The HRO rates the service provider.|...creates the given rating?||ServiceProvider|Creator (Rule 1)|
|6. The system shows the information and asks for confirmation.||||
|7. The HRO confirms.|...saves the rating?|ServiceProvider|Information Expert (IE)|
|8. The system shows a success message.||||
|

## Systematization

  From the rationale results the conceptual classes promoted to classes of software are:

* Company
* HRO
* Service Provider
* Rating

Other software classes (i.e. Pure Fabrication) identified:  

* EvaluateSPUI  
* EvaluateSPController
* ServiceProviderRegistry
* Authorization/Facade

## Sequence Diagram

![SD_UC8_LAPR.png](images/SD_UC15_LAPR.PNG)

## Class Diagram

![CD_UC8_LAPR.png](images/CD_UC15_LAPR.PNG)
