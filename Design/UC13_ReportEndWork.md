# Realization of UC13 Report End of Work

## Rationale

| Main flow | Question: Which Class... | Answer | Justification |
|:-------------------------------------------------------------------------------------------------------|:------------------------------------------------------------|:-----------------------------------------------|:---------------------------------------------------------------------------------------------------------------------|
|1. The Service Provider starts the process of deciding to set the end of the work of each service.|... interacts with the Service Provider?|ReportEndWorkUI|Pure Fabrication UI|
||... coordinates the UC?|ReportEndWorkController|Pure Fabrication Controller|
||create / instance Service ExecutionExportData ?|ReportEndWorkController| Pattern HC + LC + Creator (Rule 1)|
|2. The system shows the list of execution orders to set as concluded.| | | |
|3. The client selects the executions that whant to conclude. ||||
|4. Optionally the Service Provider can add an issue to each of the execution order.|...saves the execution orders issue and status?|ReportEndWorkController|IE: ExecutionOrder contains issue data and status |

## Systematization

From the rationale, the conceptual classes promoted to software classes are:

* Company
* Service Provider
* ExecutionOrder

Other software classes (i.e. Pure Fabrication) also identified:

* ReportEndWorkUI
* ReportEndWorkController
* ExecutionOrderRegister
* ServiceProviderRegister

## Sequence Diagram

![SD_UC13_LAPR.png](images/SD_UC13_LAPR.png)
![ref.png](images/loopExecutionData.png)

## Class Diagram

![CD_UC13_LAPR.png](images/CD_UC13_LAPR.png)
