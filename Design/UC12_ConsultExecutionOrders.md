# Realization of UC12 Consult Execution Orders

## Rationale

| Main flow | Question: Which Class... | Answer | Justification |
|:-------------------------------------------------------------------------------------------------------|:------------------------------------------------------------|:-----------------------------------------------|:---------------------------------------------------------------------------------------------------------------------|
|1. The Service Provider starts the process of consulting execution orders.|... interacts with the Service Provider?|ExecutionOrdersConsultUI|Pure Fabrication UI|
||... coordinates the UC?|ExecutionOrdersConsultController|Pure Fabrication Controller|
||create / instance ExecutionExportData ?|ExecutionOrdersConsultController| Pattern HC + LC + Creator (Rule 1)|
|2. The system shows the list of execution orders of Service Provider and its information.| | | |
|3. Optionally the Service Provider can export Data to a file. |...cordinates this action|ExecutionOrdersConsultController||
|4. The system validates, exports data and informs of the success of the operation.||||

## Systematization

From the rationale, the conceptual classes promoted to software classes are:

* Company
* Service Provider
* ExecutionOrder
* Service
* ServiceRequest
* ServiceRequestDiscription
* PostalAddress
* Client
* ExecutionsExportData

Other software classes (i.e. Pure Fabrication) also identified:

* ExecutionOrdersConsultUI  
* ExecutionOrdersConsultController
* ExecutionOrderRegister
* ServiceProviderRegister
* ServiceRequestRegister

## Sequence Diagram

![SD_UC12_LAPR.png](images/SD_UC12_LAPR.png)

## Class Diagram

![CD_UC12_LAPR.png](images/CD_UC12_LAPR.png)
