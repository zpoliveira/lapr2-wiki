# Performing UC9 - Specify Daily Availability

## Rationale

| Home | Question: What Class ... | Reply | Justification |
|:-----|:-----------|:------|:----|
| 1. The service provider starts to identify their daily availability. | ... interact with the user?|SpecifyDailyAvailabilityUI|Pure Fabrication|
|| ... coordinates the UC? | SpecifyAvailabilityDiaryController | Controller |
|| ... create / instance of Availability? | ServiceProvider | Creator (rule 2). In the MD Service Provider Specifies several Availabilities |
|| ... who knows the class ServiceProvider? | ServiceProviderRegister | HC + LC |
|| ... who knows the class RegisterPresidentServices? | Company | HC + LC |
| 2. The system requests a period (starting date and time, and end date and time) where the service provider is available to perform services. | ||
| 3. The service provider enters the requested data. | ... saves the entered data? | Availability | Information Expert (IE) - instance created in step 1 |
| 4. The system validates and displays the data for confirmation. | ... validates the data entered (local validation) | Company | IE: Availability has its own data |
| | ... validate the data entered (global validation)? | ServiceProvider | IE: ServiceProvider knows all of his Availabilities |
| 5. The service provider confirms. | ||
| 6. The system records the availability period of the service provider and informs the service provider of the success of the operation.| ... saves Availabilities? | ServiceProvider | IE: ServiceProvider knows all of his Availability |
| 7. Steps 2 to 6 are repeated until the availability of the service provider is identified.

## Systematization

 From the rationale it results that the conceptual classes promoted to classes of software are:

* Company
* ServiceProvider
* Availability

Other software classes (i.e. Pure Fabrication) identified:

* SpecifyDailyAvailabilityUI
* SpecifyDailyAvailabilityController
* ServiceProviderRegister

## Sequence Diagram

![SD_UC9_LAPR.png](images/SD_UC9_LAPR.png)

## Class Diagram

![CD_UC9_LAPR.png](images/CD_UC9_LAPR.png)
