# UC2 Application submission

## Rationale

--------

| Main Flow| Question: What class...| Response| Justification|
|------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------|-------------------------------|------------------------------------------------------------------------------------------------------------------------|
| 1. The unregistered user starts the application submission process.                                        | ...interacts with the user?                              | SubmitApplicationUI         | PureFabrication, because it is not justified to assign this responsibility to any class in the Domain Model. |
|                                                                                                                  | ...coordinates the UC?                                          | SubmitApplicationController | Controller                                                                                                             |
|                                                                                                                  | ...creates the Application?                             | Company                       | Creator (Rule 1)                                                                                                      |
| 2. The system requests the necessary data (i.e. the person's full name, vat, telephone contact and email).   | n/a                                                        |                               |                                                                                                                        |
| 3. The unregistered user inserts the requested data.                                                     | ... saves the inserted data?                          | Application                     | Information Expert (IE) - instace created in point 1.                                                                 |
| 4. The system requests a postal address.                                                                        | n/a                                                        |                               |                                                                                                                        |
| 5. The unregistered user inserts the posta address.                                                        | ...creates the Postal Address?                         | Application                   | Creator (Rule 4).                                                                                                     |
|                                                                                                                  | ... saves the inserted data?                          | PostalAddress                | Information Expert (IE) - instace created in this step.                                                                |
| 6. The system validates and saves the inserted address.                                                             | ... saves the instance of the created Postal Address?          | Candidatura                   | Information Expert (IE) - No MD a Candidatura menciona um ou mais Endereco Postal                                      |
| 7. Steps 4 and 6 are repeated until all the required postal addresses are entered (minimum 1). | n/a                                                        |                               |                                                                                                                        |
| 8. The system requests an academic qualification.                                                                 |                                                            |                               |                                                                                                                        |
| 9. The unregistered user inserts the academic qualification.                                                   | ...creates the academic qualification?                   | Application                   | Creator (Rule 1).                                                                                                     |
|                                                                                                                  | ... guarda os dados introduzidos?                          | AcademicQualification          | Information Expert (IE) - instance created in this step.                                                                |
| 10. The system validates and saves the academic qualification.                                                           | ... saves the instance of the created academic qualification?    | Application                   | Information Expert (IE) - In the DM the Application mentions academic qualifications.                                        |
| 11. Steps 8 to 10 are repeated until all academic qualifications have been entered.               | n/a                                                        |                               |                                                                                                                        |
| 12. The system requests a professional qualification.                                                             |                                                            |                               |                                                                                                                        |
| 13. The unregistered user inserts his professional qualification.                                              | ...creates the professional qualification?                | Application                   | Creator (Rule 1).                                                                                                     |
|                                                                                                                  | ... guarda os dados introduzidos?                          | ProfessionalQualification       | Information Expert (IE) - instance created in this step.                                                                |
| 14. The system validates and saves the professional qualification.                                                        | ... saves the instance of the created professional qualification? | Application                   | Information Expert (IE) - In the DM the Application mentions professional qualifications.                                     |
| 15. Steps 12 to 14 are repeated until all professional qualifications have been entered.            | n/a                                                        |                               |                                                                                                                        |
| 16. The system requests the support documents.                                                                 | n/a                                                        |                               |                                                                                                                        |
| 17. The unregistered user inserts the support document.                                                  | ...creates the support document?                  | Application                   | Creator (Rule 1).                                                                                                     |
|                                                                                                                  | ... saves the inserted data?                          | Document               | Information Expert (IE) - instance created in this step.                                                                |
| 18. The system validates and saves the support document.                                                            | ... saves the instance of the created support document?   | Application                   | Information Expert (IE) - In the DM the Application mentions the support document.                                       |
| 19. Steps 16 to 18 are repeated until all support documents have been entered.            | n/a                                                        |                               |                                                                                                                        |
| 20. The system shows the service categories available in the system.                                          | ...knows the all categories to list?              | Company                       | IE: Company aggregates all Categories.                                                                            |
| 21. The unregistered user selects the service category he will realize.                        | | |
|22. The system validates and saves the selected category.                                                                                                           | ...saves the selected category?                           | Application                   | Information Expert (IE) - In the DM the Application mentions service categories.                                         |
| 23. Steps 20 to 22 are repeated until all categories have been entered.                            |                                                            |                               |                                                                                                                        |
| 24. The system validates and shows the application data and asks for confirmation. | ...validates the Application data? (local validation)      | Application   | Information Expert (IE) - Application knows his own data.
|  | ...validates the application data? (global validation)       | Company                       | Information Expert (IE) - The company has/aggregates Application.
| 25. The unregistered user confirms the application data.                                                 |      |                       |                                                   |
| 26. The system registers the new application and shows a success message.            | ...saves the created application?             | Company                       | Information Expert (IE) – In the DM the company aggregates/has Application.                                                   |
|                                                                                                                  | ...informs the unregistered user?                     | SubmitApplicationUI         |                                                                                                                        |

## Systematization

--------

From the rationale results conceptual classes promoted to software classes like:

+ Company

+ PostalAddress

+ Application

+ AcademicQualification

+ ProfessionalQualification

+ Document

Other software classes (i.e. Pure Fabrication) identified:

+ SubmitApplicationUI

+ SubmitApplicationController

**Note:** It was attributed the responsibility of creating instances of Postal Address
to the Application. However, an application is so effective when has at least one
Postal code. In other words, to apply for a job there is absolutely no
Postal code. Thus, it is not feasible to ask to
create a Postal Address. To resolve this issue, the creation of
Postal address is made through a static method in the Application class.
This way, it is not necessary to advance an application instance.
Other proposals / alternatives are possible.

## Sequence Diagram

--------

![SD_UC2_LAPR.png](images/SD_UC2_LAPR.png)

## Class Diagram

--------

![CD_UC2_LAPR.png](images/CD_UC2_LAPR.png)
