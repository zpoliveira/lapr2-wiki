# Realization of UC11 Decide Proposed Period of Services

## Rationale

| Main flow | Question: Which Class... | Answer | Justification |
|:-------------------------------------------------------------------------------------------------------|:------------------------------------------------------------|:-----------------------------------------------|:---------------------------------------------------------------------------------------------------------------------|
|1. The Client starts the process of deciding on the proposed period of services.|... interacts with the Client?|DecideProposedPeriodOfServicesUI|Pure Fabrication UI|
||... coordinates the UC?|DecideProposedPeriodOfServicesController|Pure Fabrication Controller|
||create / instance Service Execution Orders?|ExecutionOrderRegistry| Pattern HC + LC (on Company) + Creator (Rule 1)|
|2. The system shows the services and their respective proposed periods of execution, requesting confirmation.| | | |
|3. The client confirms. ||||
|4. The system validates, **issues execution orders** and informs of the success of the operation.|...saves the execution orders?|ExecutionOrderRegistry|IE: ExecutionOrderRegistry contains / aggregates Execution Orders|

## Systematization

 From the rationale, the conceptual classes promoted to software classes are:

* Company
* Client
* Execution Orders
* Service Request
* Service Request Description
* Service Provider

Other software classes (i.e. Pure Fabrication) also identified:

* DecideProposedPeriodOfServicesUI  
* DecideProposedPeriodOfServicesController
* ExecutionOrderRegistry
* ServiceRequestRegistry

## Sequence Diagram

![SD_UC11_LAPR.png](images/SD_UC11_LAPR.png)

## Class Diagram

![CD_UC11_LAPR.png](images/CD_UC11_LAPR.png)
